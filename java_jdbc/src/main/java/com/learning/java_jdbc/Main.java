package com.learning.java_jdbc;

import java.sql.*;
import java.util.Scanner;


public class Main {
	public static void main(String args[])
	{
		Scanner scan = new Scanner(System.in);
		int value;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost:8086/student","root","root");
			System.out.print("Enter a value to insert : ");
			value = scan.nextInt();
			PreparedStatementExe.runStmt(con,value);
			StatementExe.runStmt(con);
			con.close();
			System.out.println();
			RowSetExe.runStmt();
			
		} catch (Exception e) {
			e.printStackTrace();
		}  
	}
}
