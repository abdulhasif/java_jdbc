package com.learning.java_jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PreparedStatementExe {
	public static void runStmt(Connection con,int data)
	{
		try {
			PreparedStatement prep = con.prepareStatement("insert into std values(?)");
			prep.setInt(1,data);
			prep.execute();
			System.out.println("Data inserted");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
