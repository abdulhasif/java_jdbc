package com.learning.java_jdbc;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetProvider;

public class RowSetExe
{
	public static void runStmt()
	{
		try{
			JdbcRowSet rs = RowSetProvider.newFactory().createJdbcRowSet();
			rs.setUrl("jdbc:mysql://localhost:8086/student");
			rs.setUsername("root");
			rs.setPassword("root");
			rs.setCommand("select * from std");
			rs.execute();
			while(rs.next())
			{
				System.out.print(rs.getInt(1) + " ");
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
