package com.learning.java_jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StatementExe {
	public static void runStmt(Connection con)
	{
		try 
		{
			Statement stmt = con.createStatement();
			ResultSet fetchedData = stmt.executeQuery("select * from std");
			while(fetchedData.next())
			{
				System.out.print(fetchedData.getInt(1) + " ");
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
	}
}
